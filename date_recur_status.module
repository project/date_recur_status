<?php

/**
 * @file
 * Contains date_recur_status.module.
 */

/**
 * Implements hook_theme().
 */
function date_recur_status_theme($existing, $type, $theme, $path) {
  return array(
    'date_recur_status_formatter' => [
      'variables' => [
        'occurrences' => [],
        'repeatrule' => NULL,
        'date' => NULL,
        'isRecurring' => FALSE,
      ],
    ],
  );
}

/**
 * Implements hook_search_api_index_items_alter(().
 *
 * This removes occurrences that have a status which is in the list of statuses
 * that are set to not be indexed.
 */
function date_recur_status_search_api_index_items_alter(\Drupal\search_api\IndexInterface $index, array &$items) {
  $statuses_to_remove = \Drupal::config('date_recur_status.settings')->get('skip_index');
  $occurrence_value_keys = ['status', 'value', 'end_value'];
  /** @var \Drupal\search_api\Item\Item $item */
  foreach ($items as $item_id => $item) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $item->getOriginalObject()->getValue();
    if (!empty($entity)) {
      /** @var \Drupal\search_api\Item\FieldInterface $field */
      foreach ($item as $name => $field) {
        $property_path = explode(':', $field->getPropertyPath());
        $field_name = $property_path[0];
        $field_definition = $entity->getFieldDefinition($field_name);
        // Check if current field is an occurrence status field.
        // @todo: Support nested entities.
        if (! ( !empty($field_definition) &&
            count($property_path) == 3 && $property_path[1] == 'occurrences' && $property_path[2] == 'status'
            && $field_definition->getType() == 'date_recur'
            && $field_definition->getSetting('occurrence_handler_plugin') == 'date_recur_status_occurrence_handler'
            && count($property_path) == 3 && $property_path[1] == 'occurrences' && $property_path[2] == 'status')) {
          continue;
        }

        $unset = [];
        foreach ($field->getValues() as $key => $value) {
          if (in_array($value, $statuses_to_remove)) {
            $unset[$key] = $key;
          }
        }
        if (!empty($unset)) {
          /** @var FieldInterface $occurrence_field */
          foreach ($item->getFields() as $occurrence_field) {
            $path = explode(':', $occurrence_field->getPropertyPath());
            if ($path[0] == $property_path[0] && $path[1] == 'occurrences' && in_array($path[2], $occurrence_value_keys)) {
              $values = array_diff_key($occurrence_field->getValues(), $unset);
              $occurrence_field->setValues($values);
            }
          }
        }
      }
    }
  }
}
